# Création d'une application Client ID sur le Portal

Pour enregistrer votre application et générer un identifiant, il faut créer une application sur le Portal (AGS Online ou AGS Enterprise).





### Connexion à votre Portail
Commencez par vous connecter à votre Portail.
Assurez-vous d'avoir un compte de type "Creator" ou "Gis Professional".

### Ajout d'une nouvelle application
Dans le menu **Contenu**, cliquez sur **Nouvel élément**.
Choisissez **Application**, ensuite **Autre application**.

![alt text](./images/cap1.png)
![alt text](./images/cap2.png)

Remplir les informations (titres, balises, résumé...) et enregistrer l'application.

### Informations d'enregistrement

Dans le menu **Paramètres**, 

![alt text](./images/cap3.png)

Cliquez sur  **mettre à jour**:

Ensuite, saisir l'url https://localhost:3001/ et ajouter.

![alt text](./images/cap4.png)

L'ID de l'application est généré, gardez-le et enregistrez l'application.

![alt text](./images/cap5.png)