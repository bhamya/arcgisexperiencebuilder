# Widget Personnalisable

https://developers.arcgis.com/experience-builder/guide/create-a-starter-widget/

Dans cette partie, on va décrire les étapes à suivre pour créer un widget personnalisable.

## Organisation du dossier widget

    ↳ 📄 manifest.json : pour définir les propriétés du widget
    ↳ 📄 config.json : pour définir la configuration et le paramétrage.
    ↳ 📂 src  
        ↳ 📂 runtime → pour le corps du widget
            ↳ 📄 widget.tsx : la fonction principale
            ↳ 📂 assets  
            ↳ 📂 translations  
        ↳ 📂 setting → pour définir la partie qui concerne le pramétrage du widget dans Experience Builder.
            ↳ 📄 setting.tsx : pour définir le paramétrage
            ↳ 📂 assets  
            ↳ 📂 translations   
    ↳ 📂 dist → le code compilé
    ↳ 📄 icon.svg : icône du widget

## Développement du widget

(A définir : un exemple de widget basic à développer et à commiter dans le projet).
Ce widget constituera une base pour créer d'autres widgets personnalisés.

## Intégration dans l'application

Le nouveau widget à dévelloper doit absolument être mis dans le dossier **client\your-extensions\widgets**
A la recompliation du serveur, le nouveau widget apparait dans la liste des widgets proposés.

## Pour informations:
on n'est pas obligé de compiler à chaque fois le client ni le serveur.
Le client lance la compilation automatiquement après chaque nouvelle modification.
Le serveur aussi, mais il existe des cas particuliers oû on doit recompiler à la main:
- Installation d'un nouveau module
- Ajouter/supprimer/renommer un widget
- Modifier le manifest d'un widget
- Ajouter/supprimer/renommer un fichier ou un dossier.