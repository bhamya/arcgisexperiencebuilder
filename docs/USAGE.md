# Description générale  

Une fois le serveur est lancé (via un terminal, se positionner sur le dossier **server** et taper npm start) une page s'ouvre dans le navigateur https://localhost:3001/.

Vous pouvez commencer à créer une nouvelle application.

### Choix d'un template :
On commence par choisir un template parmi ceux proposés par défaut :

![alt text](./images/cap6.png)

### Présentation des fonctionnalités :
L'interface de "Experience Builder" se compose de plusieurs parties:

- la liste des widgets : contient la liste de tous les widgets disponibles. Les widgets personnalisés apparaissent aussi dans la liste.
- la gestion des pages/fenêtres
- la source de données 
- les différents thèmes.



### Ajout d'une source de données :

Cette rubrique permet d'ajouter des données à l'application :
    - WebMap/WebScene
    - FeatureLayer
    - données à partir d'un Url...

![alt text](./images/cap7.png)

### Ajout d'un widget :

![alt text](./images/cap8.png)

### Modification du thème :

Il permet de définir un thème à l'application. Ces thèmes peuvent être personnalisables (choix de couleur, police...).

![alt text](./images/cap9.png)

### Enregistrement et publication de l'application :

On peut donner un nom à l'application, l'enregistrer et la publier.
Au cours des développements, on peut lancer l'aperçu via le bouton :

![alt text](./images/cap10.png)