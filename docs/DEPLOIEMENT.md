# Déploiement d'une application 

https://developers.arcgis.com/experience-builder/guide/deployment-topics/

Une application créée via Experience Builder peu être téléchargée et hébergée sur un serveur.
Dans la suite, on détaillera commencer mettre une application sur le serveur IIS.


### Téléchargement de l'application :
Une fois le développement fini, vous pouvez télécharger l'application :

![alt text](./images/cap11.png)

un fichier zip est généré.

Extraire le contenu du zip dans un dossier accessible sur le serveur cible.


### Création d'un Pool sur IIS :

-vCommencez par ouvrir IIS. 

- Créez un nouveau Pool

- Choisissez le dossier de l'application et valider:

![alt text](./images/cap12.png)

### Accès à l'application :

Dans un navigateur, lancer l'application via son Url.