# Experience Builder 

Ce dépot contient le code source de "Experience Builder".

Ce dépôt sera utilisé pour commencer un développement autour de l'outil Experience Builder. 

### Les branches :
- master : code source de la v1.7.0 de la solution téléchargée à partir de cette page : https://developers.arcgis.com/downloads/#arcgis-experience-builder



## Documentation

### Organisation du dépot

    ↳ 📄 README.md → ce document    
    ↳ 📂 docs → contient toute la documentation du proje
    ↳ 📂 ArcGISExperienceBuilder
        ↳ 📄 3rd-party-license.txt : la licence.
        ↳ 📄 version.json : la version de l'outil.
        ↳ 📂 client → contient toutes les composantes essentielles pour créer une application: widgets, thèmes, 
        ↳ 📂 server → héberge les applications qui seront créées 

### Installation

- Prérequis : 
    - Assurez-vous d'avoir une version de NodeJS installée sur votre machine. Pour cela :
    Ouvrir un invité de commande, et taper node.
    Si aucune version installée, télécharger et installer NodeJS : https://nodejs.org/en/download/

    - les navigateurs supportées sont :
        - Google Chrome
        - Microsoft Edge
        - Mozilla Firefox
        - Safari.

    - Avoir un IDE sur la machine (ex : Visual Studio Code).

- Etapes d'installation (https://developers.arcgis.com/experience-builder/guide/install-guide/) : 
    - Cloner ce dépôt sur votre machine.
    - Ouvrir le projet dans Visual Studio Code.

- Installation du server :
    - Créer une application Client ID sur ArcGIS Online ou sur ArcGIS Enterprise afin de générer un identifiant. [Pour plus d'informations](./docs/APP_CLIENT.md).
    - Dans un terminal, accéder au répertoire server
    - Lancer la commande 
    
            npm ci
    - Ensuite 
    
            npm start
    - Dans le navigateur, accéder à l'url https://localhost:3001/
    - Saisir l'url du portail (AGS Online ou AGS Enterprise) et l'App ID déjà généré.

- Installation du client : 
    - Dans un terminal, accéder au répertoire client
    - lancer la commande 
    
            npm ci
    - Ensuite 
    
            npm start



## Utilisation

- [Principes de fonctionnement](./docs/USAGE.md)
- [Développer un nouveau widget](./docs/NOUVEAU_WIDGET.md)

## Déploiement

- [Déployer une application sur un serveur de type IIS](./docs/DEPLOIEMENT.md)
